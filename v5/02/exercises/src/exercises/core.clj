(ns exercises.core
  (:gen-class))

(defn ask-number []
  (print "Enter a number: ")
  (flush)
  (let [^String line (read-line)
        value (try
                (Integer. line)
                (catch NumberFormatException e 0))]
    (if-not (pos? value)
      (do
        (println "Not a valid number")
        (recur))
      (println (str "Number is "
                    (if (even? value)
                      "even"
                      "odd"))))))

(defn multiples-of-3
  ([limit]
   (loop [n 3
          r []]
     (if (<= n limit)
       (recur (+ n 3) (conj r n))
       r)))
  ([]
   (iterate #(+ % 3) 3)))

(defn lotto-row []
  (let [lotto-number #(inc (rand-int 39))]
    (loop [numbers (sorted-set)]
      (if (< (count numbers) 7)
        (recur (conj numbers (lotto-number)))
        numbers))))

(defn gcd [p q]
  (if (zero? q)
    (Math/abs ^Integer p)
    (let [p%q (mod p q)]
      (recur q p%q))))

(defn -main
  [& args]
  (ask-number)
  (let [a 102
        b 68]
    (println (str (format "multiples of 3: %s\n" (multiples-of-3 15))
                  (format "lotto row: %s\n" (lotto-row))
                  (format "(gcd %d %d) => %d" a b (gcd a b))))))
