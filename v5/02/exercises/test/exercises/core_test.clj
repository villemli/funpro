(ns exercises.core-test
  (:use exercises.core
        midje.sweet))

(defn divisible-by? [n]
  #(zero? (rem % 3)))

(defn between? [min max n]
  (and (>= n min) (<= n max)))

(facts "about `multiples-of-3`"
       (every? (divisible-by? 3) (multiples-of-3 100)) => true
       (every? (divisible-by? 3) (take 100 (multiples-of-3))) => true
       (count (multiples-of-3 60)) => 20)

; TODO: test multiple times
(facts "about `lotto-row`"
       (fact "7 numbers"
             (count (set (lotto-row))) => 7)
       (fact "distinct"
             (apply distinct? (lotto-row)) => true)
       (fact "1 >= n >= 39"
             (every? #(between? 1 39 %) (lotto-row)) => true))

(tabular "about `gcd`"
         (fact
           (gcd ?p ?q) => ?res)
         ?p ?q ?res
         1 2 1
         28 42 14
         68 102 34
         -1 3 1
         1 -3 1)