(ns exercises.core-test
  (:use exercises.core)
  (:use midje.sweet))

(facts "square tehtävä 4"
       (square 2) => 4
       (square 7) => 49
       (square -3) => 9)

(tabular "leap-year? exercise 5 "
         (fact
           (leap-year? ?year) => ?expected)
         ?year ?expected
         100 false
         200 false
         400 true
         800 true
         2000 true
         2200 false
         12 true
         20 true
         15 false
         1913 false)

(facts "divisible-by?"
       (divisible-by? 1 1) => true
       (divisible-by? 1 2) => false
       (divisible-by? 1.5 1.5) => true
       (divisible-by? 0 1) => true)
