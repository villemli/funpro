(ns exercises.core
  (:gen-class))

(defn print-14 []
  (+ (* 5 2) 4))

(defn print-15 []
  (+ 1 2 3 4 5))

(def greet (fn [name] (str "Welcome to Hogwarts " name)))

(defn middle-name [full-name]
  (get-in full-name [:name :middle]))

(defn square [x]
  (* x x))

(defn divisible-by? [a b]
  (zero? (mod a b)))

(defn leap-year? [year]
  (cond
    (divisible-by? year 400) true
    (divisible-by? year 100) false
    :else (divisible-by? year 4)))

(defn -main
  [& args]
  (run! println [(print-14)
                 (print-15)
                 (greet "Harry")
                 (middle-name {:name {:first  "Urho"
                                      :middle "Kaleva"
                                      :last   "Kekkonen"}})]))
