const raiseToPower = (x, n, sum = x) => {
  if (!Number.isInteger(n)) {
    console.log('Error: exponent should be an integer');
    return false;
  }

  if (n > 0) {
    switch (n) {
    case 1:
      return sum;
    default:
      return raiseToPower(x, n - 1, sum * x);
    }
  } else if (n == 0) {
    return 1;
  } else {
    switch (n) {
      case -1:
        return 1 / sum;
      default:
        return raiseToPower(x, n + 1, sum * x);
    }
  }
}
