const compareGenerator = () => {
  return (a, b) => {
    if (a > b) {
      return 1;
    } else if (b > a) {
      return -1;
    } else {
      return 0;
    }
  }
}

const zip = arrays => {
  return arrays[0].map((_, i) => {
    return arrays.map(array => array[i]);
  });
}

const isPositive = v => v > 0;

const compareTemperaturesWith = (comparator, listA, listB) => {
  return zip([listA, listB])
    .map(pair => comparator(pair[1], pair[0]))
    .filter(isPositive)
    .length;
}

const temps2015 = [1, 2,   3, 4,   5, 6,   7, 8,   9, 10,   11, 12];
const temps2016 = [1, 2.5, 3, 4.5, 5, 6.5, 7, 8.5, 9, 10.5, 11, 12.5];

const greaterTempsIn2016 = compareTemperaturesWith(compareGenerator(), temps2015, temps2016);
