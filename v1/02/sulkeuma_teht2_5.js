'use strict';

const counter = (() => {
  let x = 1;
  return {
    inc: () => ++x,
    dec: () => --x,
  };
})();
