const isPalindrome = word => {
  if (word.length < 2) {
    return true;
  } else if (word[0] !== word[word.length - 1]) {
    return false;
  } else {
    const middlePart = word.slice(1, word.length - 1);
    return isPalindrome(middlePart);
  }
}