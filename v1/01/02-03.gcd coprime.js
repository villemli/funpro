const gcd = (p, q) => {
  if (q == 0) {
    return p;
  } else {
    return gcd(q, p % q);
  }
}

const areCoprimeLazy = (p, q) => {
  return gcd(p, q) == 1;
}

const areCoprime = (p, q) => {
  switch (q) {
    case 0:
      return false;
    case 1:
      return true;
    default:
      return areCoprime(q, p % q);
  }
}