const raiseToPower = (x, n) => {
  switch (n) {
    case 0:
      return 1;
    case 1:
      return x;
    default:
      return x * raiseToPower(x, n - 1);
  }
}