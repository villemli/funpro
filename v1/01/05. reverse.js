const reverse = ar => {
  if (ar.length == 1) {
    return ar;
  } else {
    const last = ar[ar.length - 1];
    const allButLast = ar.slice(0, ar.length - 1);
    return [last, ...reverse(allButLast)];
  }
}