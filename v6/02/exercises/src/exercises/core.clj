(ns exercises.core
  (:gen-class)
  (:require [exercises.sima :as sima]
            [clojure.pprint :as pp]))

(def informal-greeting (partial format "- %s"))
(def formal-greeting (partial format "Best Regards, %s"))

(def minimums
  "Takes a list of number lists and returns the smallest value of each."
  (partial map #(apply min %)))

(def vampire-database
  {0 {:makes-blood-puns? false :has-pulse? true :name "McFishwich"}
   1 {:makes-blood-puns? false :has-pulse? true :name "McMackson"}
   2 {:makes-blood-puns? true :has-pulse? false :name "Damon Salvatore"}
   3 {:makes-blood-puns? true :has-pulse? true :name "Mickey Mouse"}})

(defn add-to-vampire-db
  "Adds a person to a vampire database."
  [db mbp hp name]
  (let [id (inc (apply max (keys db)))]
    (assoc db id {:makes-blood-puns? mbp
                  :has-pulse?        hp
                  :name              name})))

(defn remove-from-vampire-db
  "Removes person with id `id` from vampire database `db`"
  [db id]
  (dissoc db id))

(defn make-sima
  "Make `n` portions of sima according to `recipe`"
  ([recipe n]
   (map #(update % :maara (partial * n)) recipe))
  ([recipe]
   (make-sima recipe 1)))

(defn -main
  [& _]
  (println "1)")
  (let [full-name "Mickey Mouse"
        first-name (first (clojure.string/split full-name #" "))]
    (println (str (informal-greeting first-name)
                  "\n"
                  (formal-greeting full-name)
                  "\n")))

  (println "2)")
  (println ((juxt identity (partial apply vector))
             (minimums [[1 2 3] [4 5 6] [7 8 9]]))
           "\n")

  (println "3-4)")
  (pp/pprint (-> vampire-database
                 (remove-from-vampire-db 3)
                 (add-to-vampire-db true true "Minnie Mouse")))
  (println)

  (println "5)")
  (pp/pprint (make-sima sima/omasima 3)))