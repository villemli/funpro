(ns exercises.util
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]))

(defn mean
  "Returns the mean of numbers."
  [& numbers]
  (/ (reduce + numbers) (count numbers)))

(defn nths
  "Like `nth` but for multiple indexes."
  [indexes coll]
  (map #(nth coll %) indexes))

(defn zip
  "Merges collections, like Python's zip."
  [& colls]
  (partition (count colls) (apply interleave colls)))

(defn update-with
  "Applies fs[n] to element coll[n]."
  [fs coll]
  (map #((first %) (second %)) (zip fs coll)))

(defn group-by-first
  "Groups tuples like group-by and removes the first elements from them."
  [tuples]
  (apply merge-with
         (comp flatten list)
         (map (partial apply sorted-map) tuples)))

(def str->int #(Integer/parseInt %))

(def str->double #(Double/parseDouble %))

(defn merge-keys
  "Merge two keys in map m by applying f to old keys."
  [key-a key-b newkey f m]
  (let [newvalue (f (key-a m) (key-b m))]
    (-> m
        (dissoc key-a key-b)
        (assoc newkey newvalue))))

(defn parse-csv
  "Returns file's contents split to a list of vectors.
   Each vector represents a line and contains the columns as strings."
  [filename]
  (with-open [reader (io/reader filename)]
    (doall
      (csv/read-csv reader))))