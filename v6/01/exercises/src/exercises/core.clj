(ns exercises.core
  (:gen-class)
  (:require [clojure.pprint]
            [exercises.util :refer :all]))

(defn temp-data
  "Returns a map with years as keys and lists of monthly temperatures as values."
  [csv-data]
  (->> csv-data
       rest
       (map #(nths [0 5] %))
       (map #(update-with [str->int str->double] %))
       group-by-first))

(def hki-temp-data
  "Monthly temperatures of Helsinki taken from
  https://ilmatieteenlaitos.fi/havaintojen-lataus ."
  (->> "resources/helsinki-temperatures.csv"
       parse-csv
       temp-data))

(defn positive-mean-temperature
  "Takes a list of yearly temperature lists, calculates monthly mean
  temperatures, takes the positive ones and returns the mean of those."
  [& colls]
  (->> (apply zip colls)
       (map #(apply mean %))
       (filter pos?)
       (apply mean)))

(def food-journal
  [{:month 3 :day 1 :liquid 5.3 :water 2.0}
   {:month 3 :day 2 :liquid 5.1 :water 3.0}
   {:month 3 :day 13 :liquid 4.9 :water 2.0}
   {:month 4 :day 5 :liquid 5.0 :water 2.0}
   {:month 4 :day 10 :liquid 4.2 :water 2.5}
   {:month 4 :day 15 :liquid 4.0 :water 2.8}
   {:month 4 :day 29 :liquid 3.7 :water 2.0}
   {:month 4 :day 30 :liquid 3.7 :water 1.0}])

(def other-liquids
  "A function to calculate other liquids of a day."
  (comp #(reduce - %) (juxt :liquid :water)))

(defn -main
  [& _]
  (println (positive-mean-temperature (get hki-temp-data 2015)
                                      (get hki-temp-data 2016)))
  (->> food-journal
       (filter #(= (:month %) 4))
       (map other-liquids)
       (reduce +)
       println)
  (->> food-journal
       (filter #(= (:month %) 4))
       (map #(merge-keys :liquid :water :other-liquid - %))
       clojure.pprint/pprint))
