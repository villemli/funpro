(ns exercises.util-test
  (:require [midje.sweet :refer :all]
            [exercises.util :refer :all]))

(facts "about `mean`"
       (mean 0 1) => 1/2
       (mean -1 1) => 0)

(facts "about `nths`"
       (nths [0 2] [\a \b \c]) => '(\a \c)
       (nths [] [0 1 2]) => '())

(facts "about `zip`"
       (zip [0 1 2] "abc") => '((0 \a) (1 \b) (2 \c))
       (zip [0 1 2] [8 9]) => '((0 8) (1 9)))

(facts "about `update-with`"
       (update-with [identity inc dec] [0 1 2]) => '(0 2 1)
       (update-with [first last] ["abc" "xyz"]) => '(\a \z))

(facts "about `group-by-first`"
       (group-by-first [[\a 0] [\b 1] [\a 2] [\b 3]]) => {\a '(0 2) \b '(1 3)}
       (group-by-first [[0 \a] [0 \b] [1 \c] [2 \d]]) => {0 '(\a \b) 1 \c, 2 \d})

(facts "about `merge-keys`"
       (merge-keys :a :b :ab + {:a 1 :b 2 :c 3}) => {:ab 3 :c 3}
       (merge-keys :a :b :c * {:a 2 :b 3}) => {:c 6})