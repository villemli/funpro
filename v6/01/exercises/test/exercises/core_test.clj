(ns exercises.core-test
  (:require [midje.sweet :refer :all]
            [exercises.core :refer :all]))

(facts "about `positive-mean-temperature`"
       (positive-mean-temperature [-1 0 1 2 3] [-2 -1 0 1 2]) => 3/2
       (positive-mean-temperature [1 3] [-2 2] [0 0]) => 5/3)