const Immutable = require('immutable');

const set1 = Immutable.Set(['red', 'green', 'blue']);
const set2 = set1 & 'brown';
const set3 = set2 & 'brown';


console.log(set1 === set2);
console.log('=> Sets 1 and 2 contain different values');
console.log(set2 === set3);
console.log('=> Sets 2 and 3 contain same values');
