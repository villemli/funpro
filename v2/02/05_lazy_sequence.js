const Immutable = require('immutable');

const isNotFactorOf = a => b => a % b !== 0;

const isPalindrome = w => {
  if (w.length < 2) return true;

  if (w[0] === w[w.length - 1]) {
    return isPalindrome(w.slice(1, -1));
  } else {
    return false;
  };
};

const isPrime = n => {
  return Immutable.Range(2, Math.floor(Math.sqrt(n)) + 1)
    .every(isNotFactorOf(n));
};

const primes = Immutable.Range(2).filter(isPrime);
const palindromePrimes = primes.filter(n => isPalindrome(n.toString()));

console.log(palindromePrimes.take(30).toArray());
