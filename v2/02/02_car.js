const Car = (() => {
  const hidden = new WeakMap();

  class Car {
    // gasTank = {level: 100, capacity: 100};
    constructor(newGasTank, newOdometer) {
      hidden.set(this, {odometer: newOdometer});
      this.gasTank = newGasTank;
    }
    drive() {
      if (this.gasTank.level > 0) {
        this.updateGasTank(x => --x);
        const currentOdometer = hidden.get(this).odometer;
        hidden.set(this, {odometer: currentOdometer + 1});
        console.log(`Driving, tank: ${this.gasTank.level}/${this.gasTank.capacity}`);
      } else {
        console.log('Gas tank is empty :(');
      }
    }
    fillGasTank() {
      this.updateGasTank(x => this.gasTank.capacity);
    }
    updateGasTank(func) {
      const currentLevel = this.gasTank.level;
      const capacity = this.gasTank.capacity;
      this.gasTank = {level: func(currentLevel), capacity: capacity};
    }
    get gasLevel() {
      return this.gasTank.level;
    }
    get odometer() {
      return this.odometer;
    }
  }

  return Car;
})();

const log = console.log;
const car = new Car({level: 10, capacity: 10}, 0);

log(car.gasLevel);
car.drive();
car.fillGasTank();
log(car.gasLevel);

log(car.gasTank);
car.gasTank = {level: 20, capacity: 10};
log(car.gasTank);
