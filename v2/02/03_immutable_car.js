const Car = (gasTank, odoMeter) => {
  const _gasTank = gasTank;
  const _odometer = odoMeter;
  const _updateGasLevel = func => ({
      capacity: _gasTank.capacity,
      level: func(_gasTank.level),
  });

  return {
    gasLevel: () => _gasTank.level,
    gasTank: () => _gasTank,
    odometer: () => _odometer,
    drive: () => {
      console.log('Driving...');
      if (_gasTank.level > 0) {
        return Car(_updateGasLevel(level => level - 1), _odometer - 1);
      } else {
        console.log('Tank is empty!');
        return Car(_gasTank, _odometer);
      }
    },
    fillGasTank: () => {
      console.log('Filling the tank...');
      return Car(_updateGasLevel(level => _gasTank.capacity), _odometer);
    }
  };
};

const log = console.log;
let car = Car({capacity: 10, level: 1}, 0);

log(car.gasTank());
car = car.drive();
log(car.gasTank());
car = car.drive();
log(car.gasTank());
car = car.fillGasTank();
log(car.gasTank());
car = car.drive();
log(car.gasTank());
