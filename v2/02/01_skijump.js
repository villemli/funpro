const Immutable = require('immutable');

const distancePoints = (kPoint, extraPoints) => jumpLength => {
  return 60 + (jumpLength - kPoint) * extraPoints;
};
const competitionScores = (hill, jumpLength) => jumpLength.map(hill);

// https://en.wikipedia.org/wiki/Ski_jumping_at_the_2018_Winter_Olympics_%E2%80%93_Men%27s_large_hill_individual
const olympic2018LargeQualifying = distancePoints(125, 1.8);
// 1., 10., 20., 30., 40. rank
const lengths = [135.0, 140.0, 133.0, 119.5, 111.0];

console.log(competitionScores(olympic2018LargeQualifying, lengths));
