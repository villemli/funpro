const fu = require('./fileutils');

/**
 * Takes a list of properties and return a function which
 * takes an object and returns it with only the properties
 * specified in the property list.
 */
const props = propList => {
  return obj => {
    const propObjects = propList.map(prop => ({
      [prop]: obj[prop]
    }));
    return Object.assign({}, ...propObjects);
  };
};

fu.readFileSyncWith('movies.js', data => {
  const db = eval(data);
  console.log(db.map(props(['title', 'release'])));
});
