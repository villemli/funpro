const fs = require('fs');

module.exports = {
  /** Reads a file synchronously and applies callback to its contents */
  readFileSyncWith: (filename, callback) => {
    return callback(fs.readFileSync(filename, 'utf8'));
  },
};
