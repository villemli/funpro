const fu = require('./fileutils');

/**
 * A reducer which calculates the frequency of every element in a list
 * Returns an object with elements as keys and frequencies as values
 */
const frequencies = (freqs, curr) => {
  freqs[curr] = (curr in freqs) ? freqs[curr] + 1 : 1;
  return freqs;
};

/** Returns an object sorted by its keys */
const sortedObject = obj => {
  return Object.entries(obj)
    .sort()
    .reduce((o, [k, v]) => (o[k] = v, o), {});
};

/** Returns frequencies of every word in text */
const getWordFrequencies = text => {
  const unsortedFreqs = text.match(/[\wåäö'-]+/g)
    .map(w => w.toLowerCase())
    .reduce(frequencies, {});
  return sortedObject(unsortedFreqs);
};

console.log(fu.readFileSyncWith('kalevala.txt', getWordFrequencies));
