const toCelsius = fahrenheit => (5/9) * (fahrenheit - 32);
const area = radius => Math.PI * radius * radius;
