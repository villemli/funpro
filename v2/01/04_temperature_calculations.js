/* data */
/* from https://suja.kapsi.fi/asema-taulukko.php?asema=100971 */
const temps = {
  2015: [-0.9, 0.9, 2.4, 5.3, 9.3, 13.3, 16.4, 17.5, 13.7, 6.4, 5.6, 3.3],
  2016: [-8.8, 0.3, 0.9, 4.8, 13.8, 15.3, 17.8, 16.4, 13.3, 5.6, 0.0, 0.2],
};

/* functions */
/* Zips two lists together by applying f to each pair of elements.
 * In other words: [f(a[0], b[0]), f(a[1], b[1]), ...] */
const zipWith = (f, a, b) => {
  return a.map((val, i) => f(val, b[i]));
};

const avg = (a, b) => (a + b) / 2;
const runningAvg = (avg, curr, idx) => avg + (curr - avg) / (idx + 1);
const listAvg = list => list.reduce((a, b) => a + b) / list.length;

/* main */
const positiveAverages = zipWith(avg, temps[2015], temps[2016])
  .filter(n => n > 0);
const runAvg = positiveAverages.reduce(runningAvg);
const realAvg = listAvg(positiveAverages);

console.log(`Running average: ${runAvg}`);
console.log(`Real average:    ${realAvg}`);
