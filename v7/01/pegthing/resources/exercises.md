# 2
new-board palauttaa map-rakenteen, jossa on yksi avain rivien määrälle, sekä
avain jokaiselle reiälle. Alla on esimerkkinä ensimmäinen reikä.
```clojure
1 {:pegged true,       ; onko reiässä nappula
   :connections {4 2   ; mahdolliset siirrot
                 6 3}}
```
Tästä voidaan nähdä, että reiästä 1 voi tehdä 2 siirtoa. Voidaan joko:

- hypätä reikään 4, jolloin reikä 2 tyhjenee tai
- hypätä reikään 6, jolloin reikä 3 tyhjenee


# 3
```clojure
(take 6 tri)
```
Palauttaa 6 ensimmäistä kolmiolukua, jotka ovat myös rivien viimeisten reikien
numeroita.

```
(last (take 6 tri))
```
Palauttaa 6. kolmiolukun. Tällä voidaan selvittää laudan viimeisen reiän
numero, kun tiedetään rivien määrä.


# 4
```
(defn pegged?
  "Does the position have a peg in it?"
  [board pos]
  (get-in board [pos :pegged]))
```
Haetaan `board`-mapista `pos`-avaimen arvo, joka on tässä tapauksessa toinen
map-rakenne, joka kuvaa reikää numero `pos`. Haetaan tästä sisemmästä mapista
`:pegged`-avaimen arvo, joka kertoo onko kyseisessä reiässä nappulaa (`true`) vai
onko onko se tyhjä (`false`).

```clojure
(defn remove-peg
  "Take the peg at given position out of the board"
  [board pos]
  (assoc-in board [pos :pegged] false))
```
`board`-mapin sisällä on `pos`-avaimen arvona toinen map-rakenne, joka kuvaa
reikää numero `pos`. Tämän sisemmässä mapissa on `:pegged`-avain, joka
määrittelee onko reiässä nappulaa. Asetetaan tämän `:pegged`-avaimen arvoksi
`false`, jolloin reikä muuttuu tyhjäksi. Lopuksi palautetaan tämä uusi versio
`board`-mapista.

```clojure
(defn place-peg
  "Put a peg in the board at given position"
  [board pos]
  (assoc-in board [pos :pegged] true))
```
Tehdään samoin kuin aiemmassa funktiossa, mutta `:pegged`-avaimen arvoksi
asetetaankin `true`, joka asettaa reikään kuvitteellisen nappulan (mikäli siinä
ei vielä sellaista ole).

```clojure
(defn move-peg
  "Take peg out of p1 and place it in p2"
  [board p1 p2]
  (place-peg (remove-peg board p1) p2))
```
Tyhjennetään reikä `p1` laudasta `board` `remove-peg`-funktiolla, joka
palauttaa uuden laudan, jossa reikä `p1` on tyhjänä. Asetetaan tähän uuteen
lautaan nappula reikään `p2` `place-peg`-funktiota käyttämällä, jonka tuloksena
saamme lopullisen laudan, joka palautetaan funktiosta.

```clojure
(let [board (remove-peg (new-board 5) 4)]
  (map #(valid-moves board %) [1 11]))
```
Tyhjennetään reikä 4, jonka jälkeen rei'istä 1 ja 11 pystyy hyppäämään siihen.


# 5
```clojure
(defn valid-move?
  "Return jumped position if the move from p1 to p2 is valid, nil
  otherwise"
  [board p1 p2]
  (get (valid-moves board p1) p2))
```
Haetaan ensin kaikki lailliset hypyt, jotka voidaan tehdä reiästä `p1` käsin.
Tähän käytetään `valid-moves`-funktiota, joka palauttaa map-rakenteen, jossa
avaimet ovat reikiä joihin hypätään, ja arvot ovat reikiä joiden yli hypätään.
Jotta reikästä `p1` voidaan hypätä reikään `p2`, pitää tästä mapista löytyä
avain `p2`. Tämä voidaan testata `get`-funktiolla hakemalla avainta `p2`
siirtomapista, joka palauttaa joko `nil` mikä tulkitaan `falsy`ksi, tai avaimen
arvon mikä tulkitaan `truthy`ksi. Tämä totuusarvo on myös funktion paluuarvo.
```clojure
(defn make-move
  "Move peg from p1 to p2, removing jumped peg"
  [board p1 p2]
  (if-let [jumped (valid-move? board p1 p2)]
    (move-peg (remove-peg board jumped) p1 p2)))
```
Funktio `valid-move?` palauttaa joko `nil`in tai reiän, jonka yli hypätään.
Mikäli tulos on `nil`, ei tehdä mitään, sillä `if-let` ei sido muuttujia
arvoihin niiden ollessa niiden ollessa `falsy`. Muussa tapauksessa laudalta
poistetaan ensin nappula, jonka yli hypätään, jonka tuloksena saadaan uusi
lauta. Tämän jälkeen tätä uutta lautaa käytetään argumenttina
`move-peg`-funktiolle, jolla siirretään nappula reiässä `p1` reikään `p2`.
Tuloksena saatu lauta toimii funktion paluuarvona.
```clojure
(defn can-move?
  "Do any of the pegged positions have valid moves?"
  [board]
  (some (comp not-empty (partial valid-moves board))
        (map first (filter #(get (second %) :pegged) board))))

```
Lauta on map-rakenne, joka sisältää avaimen rivien määrälle, sekä avaimen
jokaiselle reiälle. Reiän arvona on map-rakenne, joka sisältää muun muassa
avaimen `:pegged`, joka määrittelee onko reiässä nappulaa. Voimme käyttää
`filter`iä hakemaan kaikki reiät, joiden `:pegged`-arvo on `true`. Kun
`filter`iä käytetään mapin kanssa, palauttaa se avain-arvo -parin. Tässä
tapauksessa saamme sekä reiän numeron, että reiän ominaisuudet sisältävän
mapin. Saamme mapista pelkät avaimet käyttämällä `map`-funktiota. Nyt meillä
on lista nappulallisista rei'istä.

Seuraavaksi varmistetaan, että ainakin joistain rei'istä voidaan tehdä
laillisia siirtoja käyttäen `some`-funktiota. Se toimii hiukan samalla tavalla
kuin `filter`, joten sitäkin varten tarvitaan predikaattifunktio, sekä
tarkistettava lista. Listan olemme jo saaneet valmiiksi, joten tarvitsemme
enää funktion, joka tarkistaa voiko jostain tietystä reiästä tehdä siirtoja.
Voimme hyödyntää `valid-moves`-funktiota, joka hakee listan mahdollisista
siirroista, ja yhdistää sen `comp`-funktiolla `not-empty`-funktioon, joka
kertoo onko listassa alkioita.

Kunhan laudalta löytyy edes 1 reikä, josta voi tehdä siirtoja, palauttaa `some`
arvon `true`. Mikäli siirtoja ei löydy, palauttaa se arvon `false`.