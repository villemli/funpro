package exercises;

import menu.Dish;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    // exercise 1
    private static DoubleUnaryOperator toCelcius = f -> (5.0 / 9) * (f - 32);
    private static DoubleUnaryOperator area = r -> Math.PI * r * r;

    // exercise 2
    private static Map<Dish.Type, Long> dishCount() {
        BiFunction<Map<Dish.Type, Long>, Dish.Type, Map<Dish.Type, Long>> accumulator;
        BinaryOperator<Map<Dish.Type, Long>> combiner;

        accumulator = (acc, curr) -> {
            acc.merge(curr, 1L, Long::sum);
            return acc;
        };
        combiner = (a, b) -> Stream.concat(a.entrySet().stream(),
                                           b.entrySet().stream())
                                   .collect(Collectors.toMap(Map.Entry::getKey,
                                                             Map.Entry::getValue));
        return Dish.menu.stream()
                        .map(Dish::getType)
                        //.collect(frequencies());
                        .reduce(new TreeMap<>(), accumulator, combiner);
    }

    // exercise 3
    private static int randomInt(int min, int max) {
        return (int) (Math.random() * (max - min + 1) + min);
    }

    private static int diceThrow() {
        return randomInt(0, 6);
    }

    // exercise 4
    private static <T> List<List<T>> combinations(List<T> a, List<T> b) {
        return a.stream()
                .flatMap(e -> b.stream()
                               .map(f -> List.of(e, f)))
                .collect(Collectors.toList());
    }

    // exercise 5
    private static Stream<String> findWords(String text) {
        return Pattern.compile("[\\p{L}']+")
                      .matcher(text)
                      .results()
                      .map(MatchResult::group);
    }

    private static <T> Collector<T, ?, Map<T, Long>> frequencies() {
        return Collectors.groupingBy(Function.identity(),
                                     TreeMap::new,
                                     Collectors.counting());
    }

    public static void main(String... args) {
        System.out.println("Exercise 1");
        System.out.printf("%.2f°F = %.2f°C\n\n",
                          50.0,
                          toCelcius.applyAsDouble(50.0));
        System.out.printf("r = %.2f\nA = %.2f\n\n",
                          10.0,
                          area.applyAsDouble(10.0));

        System.out.println("Exercise 2");
        System.out.println(dishCount());
        System.out.println();

        System.out.println("Exercise 3");
        var sixesIn20Throws = Stream.generate(Main::diceThrow)
                                    .limit(20)
                                    .peek(System.out::print)
                                    .filter(n -> n == 6)
                                    .count();
        System.out.println();
        System.out.println(sixesIn20Throws);
        System.out.println();

        System.out.println("Exercise 4");
        var listA = List.of('a', 'b', 'c');
        var listB = List.of('x', 'y');
        System.out.printf("%s\n%s\n%s\n\n", listA, listB, combinations(listA, listB));

        System.out.println("Exercise 5");
        try {
            var wordFrequencies = Files.lines(Paths.get("kalevala.txt"), Charset.defaultCharset())
                                       .flatMap(Main::findWords)
                                       .map(String::toLowerCase)
                                       .collect(frequencies());
            System.out.println(wordFrequencies);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
