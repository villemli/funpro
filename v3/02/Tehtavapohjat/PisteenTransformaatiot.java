import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;

public class PisteenTransformaatiot {
          
    public static void main(String[] args) {
               
       Function<Piste, Piste> siirto = Piste.makeSiirto(1, 2);
       Function<Piste, Piste> skaalaus = Piste.makeSkaalaus(2);
       Function<Piste, Piste> kierto = Piste.makeKierto(Math.PI);
       Function<Piste, Piste> muunnos = siirto.andThen(skaalaus).andThen(kierto);
       
       Piste[] pisteet = {new Piste(1,1), new Piste(2,2), new Piste(3,3)};
       List<Piste> uudetPisteet = new CopyOnWriteArrayList();
       
       for (Piste p: pisteet){
           uudetPisteet.add(muunnos.apply(p));
       } 
  
       uudetPisteet.forEach(p -> System.out.println(p));
    }
}