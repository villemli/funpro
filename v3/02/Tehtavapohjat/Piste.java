import java.util.function.UnaryOperator;

class Piste {
    private double x;
    private double y;

    Piste(double x, double y) {
        this.x = x;
        this.y = y;
    }

    static UnaryOperator<Piste> makeKierto(double r) {
        return p -> new Piste(p.getX() * Math.cos(r) - p.getY() * Math.sin(r),
                              p.getY() * Math.sin(r) + p.getY() * Math.cos(r));
    }

    static UnaryOperator<Piste> makeSiirto(double dx, double dy) {
        return p -> new Piste(p.getX() + dx, p.getY() + dy);
    }

    static UnaryOperator<Piste> makeSkaalaus(double n) {
        return p -> new Piste(p.getX() * n, p.getY() * n);
    }

    double getY() {
        return y;
    }

    double getX() {
        return x;
    }

    @Override
    public String toString() {
        return String.format("(%.1f; %.1f)", this.getX(), this.getY());
    }
}
