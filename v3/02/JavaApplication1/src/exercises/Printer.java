package exercises;

import java.util.function.Supplier;

class Printer {
    <T> void print(Supplier<T> supplier) {
        System.out.println(supplier.get());
    }
}
