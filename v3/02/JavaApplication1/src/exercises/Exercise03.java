package exercises;

import java.util.function.Supplier;
import java.util.stream.Stream;

public class Exercise03 implements Runnable {
    private int randomInt(int min, int max) {
        return (int) (Math.random() * (max - min + 1) + min);
    }

    @Override
    public void run() {
        Stream<Integer> lotteryRowWithLambda = Stream.generate(() -> randomInt(1, 40)).limit(8);

        Stream<Integer> lotteryRowWithClass = Stream.generate(new Supplier<Integer>() {
            @Override
            public Integer get() {
                return randomInt(1, 40);
            }
        }).limit(8);


        System.out.println(Util.streamToString(lotteryRowWithLambda));
        System.out.println(Util.streamToString(lotteryRowWithClass));
    }
}
