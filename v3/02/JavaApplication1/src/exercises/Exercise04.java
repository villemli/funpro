package exercises;

import java.util.function.Supplier;
import java.util.stream.Stream;

public class Exercise04 implements Runnable {
    private Supplier<Integer> fibonacci = new Supplier<>() {
        private int a = 0;
        private int b = 1;

        @Override
        public Integer get() {
            int r = a;
            a = b;
            b += r;
            return r;
        }
    };

    @Override
    public void run() {
        System.out.println(Util.streamToString(Stream.generate(fibonacci)
                                                     .limit(10)));
    }
}
