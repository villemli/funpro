package exercises;

import java.util.function.Supplier;

public class Exercise01 implements Runnable {

    @Override
    public void run() {
        Supplier<Integer> three = () -> 3;
        Supplier<Integer> diceThrow = () -> (int) (Math.random() * 6 + 1);
        Printer printer = new Printer();

        printer.print(three);
        printer.print(diceThrow);
        printer.print(() -> 100);
    }
}
