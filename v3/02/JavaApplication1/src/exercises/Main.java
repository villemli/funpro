package exercises;

import java.util.List;

public class Main {
    public static void main(String ...args) {
        List<Runnable> exercises = List.of(
            new Exercise01(),
            new Exercise03(),
            new Exercise04()
        );

        exercises.forEach(e -> {
            System.out.println(e.getClass().getSimpleName());
            e.run();
            System.out.println();
        });
    }
}
