package exercises;

import java.util.stream.Stream;

class Util {
    static <T> String streamToString(Stream<T> stream) {
        return stream.map(String::valueOf)
                     .reduce((a, b) -> a + " " + b)
                     .orElse("");
    }
}
