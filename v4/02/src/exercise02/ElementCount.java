package exercise02;

import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class ElementCount<T> implements Collector<T, ElementCount.Counter, Long> {
    class Counter {
        long value;

        Counter() {
            this(0);
        }

        Counter(long value) {
            this.value = value;
        }

        Counter combine(Counter other) {
            return new Counter(this.value + other.value);
        }
    }

    @Override
    public Supplier<ElementCount.Counter> supplier() {
        return Counter::new;
    };

    @Override
    public BiConsumer<ElementCount.Counter, T> accumulator() {
        return (a, b) -> a.value++;
    }

    @Override
    public BinaryOperator<ElementCount.Counter> combiner() {
        // the combiner seems to be required when doing parallel processing
        return Counter::combine;
    }

    @Override
    public Function<ElementCount.Counter, Long> finisher() {
        return counter -> counter.value;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Set.of(Characteristics.UNORDERED);
    }
}
