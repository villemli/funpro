package exercise02;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.LongSupplier;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<Long> inputs = List.of(100_000L,
                                    1_000_000L,
                                    10_000_000L,
                                    100_000_000L);

        compareFunctions(Main.countElements(),
                         Main.countElementsParallel(),
                        "serial",
                        "parallel",
                         inputs);
    }

    private static <T> void compareFunctions(Consumer<T> fa, Consumer<T> fb, String labelA, String labelB, List<T> inputs) {
        inputs.forEach(in -> {
            var ra = benchmark(10, fa, in);
            var rb = benchmark(10, fb, in);
            System.out.printf("in: %12s | %s: %4d | %s: %4d | diff: %.2f%%\n",
                              in, labelA, ra, labelB, rb, 100.0 * (rb - ra) / rb);
        });
    }

    private static Consumer<Long> countElements() {
        return n -> Stream.generate(Object::new)
                          .limit(n)
                          .collect(new ElementCount<>());
    }

    private static Consumer<Long> countElementsParallel() {
        return n -> Stream.generate(Object::new)
                          .limit(n)
                          .parallel()
                          .collect(new ElementCount<>());
    }

    private static <T> long benchmark(int measurements, Consumer<T> f, T input) {
        LongSupplier oneRun = () -> {
            long startTime = System.nanoTime();
            f.accept(input);
            return (System.nanoTime() - startTime) / 1_000_000;
        };

        return LongStream.generate(oneRun)
                         .limit(measurements)
                         .min()
                         .orElse(0);
    }
}
