package exercise08;

import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        NewsAgency newsAgency = new NewsAgency();
        var newsPapers = List.of(new NewsPaper("HS", Set.of("politics")),
                                 new NewsPaper("IS", Set.of("celebrity")));

        newsPapers.forEach(newsAgency::addObserver);

        var thread = new Thread(newsAgency);

        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        newsPapers.forEach(np -> System.out.printf("%s\n%s\n\n", np, np.getHeadlines()));
    }
}
