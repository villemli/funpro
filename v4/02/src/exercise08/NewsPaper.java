package exercise08;

import java.util.*;
import java.util.stream.Collectors;

public class NewsPaper implements Observer {
    private String name;
    private Set<String> interests;
    private List<NewsItem> newsItems;

    NewsPaper(String name, Set<String> interests) {
        this.name = name;
        this.interests = interests;
        this.newsItems = new ArrayList<>();
    }

    String getHeadlines() {
        return newsItems.stream()
                        .map(NewsItem::getHeadline)
                        .collect(Collectors.joining("\n"));
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public void update(Observable o, Object arg) {
        var newsItem = (NewsItem) arg;
        if (newsItem.isAbout(interests)) {
            newsItems.add(newsItem);
        }
    }
}
