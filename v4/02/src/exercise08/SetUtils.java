package exercise08;

import java.util.HashSet;
import java.util.Set;

class SetUtils {
    static <T> Set<T> intersection(Set<T> a, Set<T> b) {
        Set<T> is = new HashSet<>(a);
        is.retainAll(b);
        return is;
    }
}
