package exercise08;

import java.util.Set;

public class NewsItem {
    private String headline;
    private Set<String> keywords;


    NewsItem(String headline, Set<String> keywords) {
        this.headline = headline;
        this.keywords = keywords;
    }

    boolean isAbout(Set<String> keywords) {
        return !SetUtils.intersection(this.keywords, keywords)
                        .isEmpty();
    }

    String getHeadline() {
        return this.headline;
    }

    @Override
    public String toString() {
        return String.format("%s\nKeywords: %s\n", getHeadline(), keywords);
    }
}
