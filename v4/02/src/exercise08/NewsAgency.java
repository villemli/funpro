package exercise08;

import java.util.List;
import java.util.Observable;
import java.util.Set;

import static java.lang.Thread.sleep;

public class NewsAgency extends Observable implements Runnable {
    @Override
    public void run() {
        var newsItems = List.of(new NewsItem("Celebrity gets drunk", Set.of("celebrity", "alcohol")),
                                new NewsItem("New law gets approved", Set.of("politics", "law")),
                                new NewsItem("Celebrity surprised about the new law", Set.of("celebrity", "law")),
                                new NewsItem("Celebrity enters politics", Set.of("celebrity", "politics")),
                                new NewsItem("Most people support the new law", Set.of("politics", "law")),
                                new NewsItem("Celebrity has an opinion", Set.of("celebrity")),
                                new NewsItem("Politician has an opinion", Set.of("politics")),
                                new NewsItem("Something boring happened", Set.of()));
        newsItems.forEach(n -> {
            try {
                setChanged();
                notifyObservers(n);
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}
