package exercise04;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Stream<Omena> omenaStream = Stream.of(new Omena("vihreä", 100),
                                              new Omena("punainen", 150));
        List<Omena> omenaLista = omenaStream.collect(
                // supplier
                ArrayList::new,
                // accumulator
                List::add,
                // combiner
                List::addAll);

        System.out.println(omenaLista);
    }
}
