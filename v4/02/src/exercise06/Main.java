package exercise06;

import java.util.Map;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        UnaryOperator<String> removeSpaces = s -> s.replaceAll(" +", " ");
        UnaryOperator<String> removeUmlauts = s -> replaceMultiple(s, Map.of("å", "a",
                                                                             "ä", "a",
                                                                             "ö", "o",
                                                                             "Å", "A",
                                                                             "Ä", "A",
                                                                             "Ö", "O"));
        UnaryOperator<String> fixSpelling = s -> s.replaceAll("sturct", "struct");
        Function<String, String> chain = removeSpaces.andThen(removeUmlauts)
                                                     .andThen(fixSpelling);

        System.out.println(chain.apply("Here's some umlauts: åäöÅÄÖ.  There's also a typo:    sturct "));
    }

    private static String replaceMultiple(String input, Map<String, String> replacements) {
        // https://stackoverflow.com/a/7661573
        String re = String.join("|", replacements.keySet());
        StringBuffer sb = new StringBuffer();
        Pattern p = Pattern.compile(re);
        Matcher m = p.matcher(input);

        while (m.find()) {
            m.appendReplacement(sb, replacements.get(m.group()));
        }
        m.appendTail(sb);

        return sb.toString();
    }
}
