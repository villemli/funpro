package exercise05;

public class LactoseFreeMilk implements Milk {
    @Override
    public String getName() {
        return "lactose-free milk";
    }

    @Override
    public void makeMilkshake() {
        System.out.println("Drinking " + getName() + "shake");
    }
}
