package exercise05;

class LactoseFreeCheese implements Cheese {
    @Override
    public String getName() {
        return "lactose-free cheese";
    }

    @Override
    public void slice() {
        System.out.println("Slicing " + getName());
    }
}
