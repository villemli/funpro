package exercise05;

import java.util.function.Supplier;

abstract class AbstractDairy {
    Supplier<Product> cheeseSupplier;
    Supplier<Product> milkSupplier;
    Supplier<Product> yogurtSupplier;

    Product getMilk() {
        return milkSupplier.get();
    }

    Product getCheese() {
        return cheeseSupplier.get();
    }

    Product getYogurt() {
        return yogurtSupplier.get();
    }
}
