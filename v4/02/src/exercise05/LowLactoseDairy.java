package exercise05;

class LowLactoseDairy extends AbstractDairy {
    LowLactoseDairy() {
        cheeseSupplier = LowLactoseCheese::new;
        milkSupplier = LowLactoseMilk::new;
        yogurtSupplier = LowLactoseYogurt::new;
    }
}
