package exercise05;

interface Milk extends Product {
    void makeMilkshake();
}
