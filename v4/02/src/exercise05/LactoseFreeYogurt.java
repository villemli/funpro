package exercise05;

class LactoseFreeYogurt implements Yogurt {
    @Override
    public String getName() {
        return "lactose-free yogurt";
    }

    @Override
    public void eat() {
        System.out.println("Eating " + getName());
    }
}
