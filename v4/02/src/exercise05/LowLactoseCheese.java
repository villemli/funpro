package exercise05;

class LowLactoseCheese implements Cheese {
    @Override
    public String getName() {
        return "low-lactose cheese";
    }

    @Override
    public void slice() {
        System.out.println("Slicing " + getName());

    }
}
