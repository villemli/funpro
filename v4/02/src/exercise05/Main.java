package exercise05;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        AbstractDairy lowLactoseDairy = new LowLactoseDairy();
        AbstractDairy lactoseFreeDairy = new LactoseFreeDairy();

        System.out.println("Low-lactose products:");
        getProducts(lowLactoseDairy);

        System.out.println();

        System.out.println("Lactose-free products:");
        getProducts(lactoseFreeDairy);

        System.out.println();

        System.out.println("Milkshakes:");
        makeMilkShake(lowLactoseDairy);
        makeMilkShake(lactoseFreeDairy);

        System.out.println();

        System.out.println("Slicing cheese:");
        sliceCheese(lowLactoseDairy);
        sliceCheese(lactoseFreeDairy);

        System.out.println();

        System.out.println("Eating yogurt:");
        eatYogurt(lowLactoseDairy);
        eatYogurt(lactoseFreeDairy);
    }

    private static void eatYogurt(AbstractDairy dairy) {
        ((Yogurt) dairy.getYogurt()).eat();
    }

    private static void getProducts(AbstractDairy dairy) {
        List.of(dairy.getCheese(),
                dairy.getMilk(),
                dairy.getYogurt())
            .stream()
            .map(Product::getName)
            .forEach(System.out::println);
    }

    private static void makeMilkShake(AbstractDairy dairy) {
        ((Milk) dairy.getMilk()).makeMilkshake();
    }

    private static void sliceCheese(AbstractDairy dairy) {
        ((Cheese) dairy.getCheese()).slice();
    }
}
