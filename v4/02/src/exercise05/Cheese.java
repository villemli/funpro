package exercise05;

interface Cheese extends Product {
    void slice();
}
