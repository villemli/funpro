package exercise05;

class LowLactoseMilk implements Milk {
    @Override
    public String getName() {
        return "low-lactose milk";
    }

    @Override
    public void makeMilkshake() {
        System.out.println("Drinking " + getName() + "shake");
    }
}
