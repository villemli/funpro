package exercise05;

class LowLactoseYogurt implements Yogurt {
    @Override
    public String getName() {
        return "low-lactose yogurt";
    }

    @Override
    public void eat() {
        System.out.println("Eating " + getName());
    }
}
