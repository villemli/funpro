package exercise05;

class LactoseFreeDairy extends AbstractDairy {
    LactoseFreeDairy() {
        cheeseSupplier = LactoseFreeCheese::new;
        milkSupplier = LactoseFreeMilk::new;
        yogurtSupplier = LactoseFreeYogurt::new;
    }
}
