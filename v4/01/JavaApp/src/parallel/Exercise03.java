package parallel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.LongStream;

import static parallel.ParallelStreamsHarness.measurePerf;

public class Exercise03 {
    public static void main(String[] args) {
        var n = 10_000_000L;
        var linkedList = fillList(new LinkedList<>(), n);
        var arrayList = fillList(new ArrayList<>(), n);

        System.out.println("Sum of squares with LinkedList done in: " + measurePerf(ParallelStreams::sumOfSquaresWithList, linkedList) + " msecs");
        System.out.println("Sum of squares with ArrayList done in: " + measurePerf(ParallelStreams::sumOfSquaresWithList, arrayList) + " msecs");
        System.out.println("Sum of squares with Stream done in: " + measurePerf(ParallelStreams::sumOfSquaresWithStream, n) + " msecs");
    }

    public static List<Long> fillList(List<Long> list, long n) {
        LongStream.rangeClosed(0, n)
                  .forEach(list::add);
        return list;
    }
}
