package parallel;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.*;

public class Exercise04 {
    public static void main(String[] args) {
        var filename = "data\\pg7000.txt";
        Function<List<?>, Map<?, Long>> wordFrequencies = list -> list.stream()
                                                                      .collect(frequencies());
        Function<List<?>, Map<?, Long>> wordFrequenciesParallel = list -> list.parallelStream()
                                                                              .collect(frequencies());

        System.out.println("Calculating word frequencies");
        IntStream.iterate(1, n -> n * 10)
                 .limit(3)
                 .forEachOrdered(n -> {
                     var words = generateWordsFrom(filename, n);
                     System.out.printf("%d words\n", words.size());
                     var timeParallel = runBenchmark(wordFrequenciesParallel, words, "parallel");
                     var timeNonParallel = runBenchmark(wordFrequencies, words, "non-parallel");
                     System.out.printf("Difference: %.2f%%\n\n",
                                       100.0 * (timeNonParallel - timeParallel) / timeNonParallel);
                 });
    }

    private static List<String> extractWords(String filename) {
        try {
            return Files.lines(Paths.get(filename), Charset.defaultCharset())
                        .flatMap(Exercise04::getWords)
                        .map(String::toLowerCase)
                        .collect(Collectors.toUnmodifiableList());
        } catch (IOException e) {
            e.printStackTrace();
            return List.of();
        }
    }

    private static <T> Collector<T, ?, Map<T, Long>> frequencies() {
        return Collectors.groupingBy(Function.identity(),
                                     TreeMap::new,
                                     Collectors.counting());
    }

    private static List<String> generateWordsFrom(String filename, int n) {
        return Stream.generate(() -> extractWords(filename))
                     .limit(n)
                     .flatMap(Collection::stream)
                     .collect(Collectors.toUnmodifiableList());
    }

    private static Stream<String> getWords(String text) {
        return Pattern.compile("[\\p{L}']+")
                      .matcher(text)
                      .results()
                      .map(MatchResult::group);
    }

    private static <T, R> long measureExecutionTime(Function<T, R> f, T input) {
        LongSupplier executionTime = () -> {
            long start = System.nanoTime();
            f.apply(input);
            return (System.nanoTime() - start) / 1_000_000;
        };
        return LongStream.generate(executionTime)
                         .limit(10)
                         .min()
                         .orElse(0);
    }

    private static <T, R> long runBenchmark(Function<T, R> f, T input, String label) {
        long time = measureExecutionTime(f, input);
        System.out.printf("%s: %dms\n",
                          label,
                          time);
        return time;
    }
}
