package testing;

import java.util.stream.IntStream;

public class Exercise02 {
    public static void main(String[] args) {
        //  sum of the triples of even integers from 2 to 10
        System.out.printf(
                "Sum of the triples of even integers from 2 to 10 is: %d%n",
                IntStream.rangeClosed(1, 10)
                         // a) 10x
                         .filter(x -> x % 2 == 0)
                         // b) 5x
                         .map(x -> x * 3)
                         // 5x
                         .sum());

        // filter after map
        System.out.printf(
                "Sum of the triples of even integers from 2 to 10 is: %d%n",
                IntStream.rangeClosed(1, 10)
                         // c) 10x
                         .map(x -> x * 3)
                         // 10x
                         .filter(x -> x % 2 == 0)
                         // 5x
                         .sum());
    }

}
